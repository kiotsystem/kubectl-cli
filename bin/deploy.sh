#!/bin/bash

set -e

echo "---- deploy from ./.generated/ ----"

export K8S_CLUSTER_ENDPOINT=192.168.254.5:6443
echo "$K8S_CLUSTER_ENDPOINT"
kubectl apply -f ./.generated/ -R
