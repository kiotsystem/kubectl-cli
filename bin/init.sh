#!/bin/bash

set -e

if [ -z "${K8S_CLUSTER}" ]; then
  echo 'K8S_CLUSTER is not set'
  exit 1
fi

if [ -z "${K8S_CLUSTER_ENDPOINT}" ]; then
  echo 'K8S_CLUSTER_ENDPOINT is not set'
  exit 1
fi

if [ -z "${K8S_USER}" ]; then
  echo 'K8S_USER is not set'
  exit 1
fi

if [ -z "${K8S_TOKEN}" ]; then
  echo 'K8S_TOKEN is not set'
  exit 1
fi

echo "---- Set Cluster ${K8S_CLUSTER} to ${K8S_CLUSTER_ENDPOINT} ----"
kubectl config set-cluster ${K8S_CLUSTER} \
               --server=${K8S_CLUSTER_ENDPOINT} \
               --insecure-skip-tls-verify=true

echo '---- Set the credentials ----'
kubectl config set-credentials ${K8S_USER} --token=${K8S_TOKEN}

echo "---- Set context for ${K8S_CLUSTER} ----"
kubectl config set-context default-context --cluster=${K8S_CLUSTER} \
                           --user=${K8S_USER}

echo '---- Use default-context ----'
kubectl config use-context default-context


echo '---- Finished init ----'
