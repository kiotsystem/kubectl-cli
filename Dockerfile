FROM debian:stretch-slim

RUN apt-get update && apt-get install -y apt-transport-https curl wget gnupg && \
    curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add - && \
    touch /etc/apt/sources.list.d/kubernetes.list && \
    echo "deb http://apt.kubernetes.io/ kubernetes-xenial main" | tee -a /etc/apt/sources.list.d/kubernetes.list && \
    apt-get update && \
    rm -rf /var/lib/apt/lists/*

RUN apt-get update && \
    apt-get install -y kubectl gettext && \
    rm -rf /var/lib/apt/lists/*

# -- Add custom scripts -- #

WORKDIR /app

ENV PATH "${PATH}:/app/bin"

COPY ./bin/ ./bin/

