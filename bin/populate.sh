#!/bin/bash

set -e

if [ -z "${BRANCH}" ]; then
  echo 'BRANCH is not defined'

  if [ ! -z "${DRONE_BRANCH}" ]; then
    export BRANCH="${DRONE_BRANCH}"
    echo "Using Drone's branch: ${BRANCH}"
  else
    exit 1
  fi
fi

if [ -z "${TAG}" ]; then
  echo 'TAG is not defined'

  echo "Falling back to use BRANCH: ${BRANCH}"
  export TAG="${BRANCH}"

  if [ ! -z "${COMMIT}" ]; then
    TAG="${COMMIT}"
    echo "Using commit sha ${TAG}"
  else
    if [ ! -z "${DRONE_TAG}" ]; then
      TAG="${DRONE_TAG}"
      echo "Using Drone's tag: ${TAG}"
    fi
  fi
fi

if [ -z "${REPO}" ]; then
  echo 'REPO is not defined'

  if [ ! -z "${DRONE_REPO}" ]; then
    export REPO="${DRONE_REPO}"
    echo "Using Drone's REPO: ${DRONE_REPO}"
  else
    exit 1
  fi
fi

if [ -z "${REPO_NAME}" ]; then
  echo 'REPO_NAME is not defined'

  if [ ! -z "${DRONE_REPO_NAME}" ]; then
    export REPO_NAME="${DRONE_REPO_NAME}"
    echo "Using Drone's Repo name: ${REPO_NAME}"
  else
    exit 1
  fi
fi

if [ -d ".generated" ]; then
  rm -rf .generated
fi

echo '---- Will copy the directory ----'
cp -vr deployment .generated
echo '---- directory copied ----'

echo '---- Populating config with env ----'

for f in deployment/*.yml
do
  envsubst < ${f} > .generated/`basename ${f}`
done

echo '---- Populated config with env ----'
